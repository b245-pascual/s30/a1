// Activity s30 Aggregation on the fruits database

//Solution 1 get the fruitOnSale

	

	db.fruits.aggregate([ {$match: {onSale: true}},{$count: "fruitsOnSale"} ]);

//Solution 2 get the enoughStock
	

	db.fruits.aggregate([
		{$match: {stock: {$gte:20}}},
		{$count: "enoughStock"}])



//Solution 3 get the average price


	db.fruits.aggregate([
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id", avgPrice:{$avg: "$price"} }}
    ])


//Solution 4 get the highest price


	db.fruits.aggregate([   
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id", maxPrice:{$max: "$price"} }},
        {$sort: {maxPrice:1}}
    ])


//Solution 5 get the lowest price

	db.fruits.aggregate([
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id", minPrice:{$min: "$price"} }},
        {$sort: {minPrice:1}}
    ])


